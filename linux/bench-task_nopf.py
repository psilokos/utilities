#! /usr/bin/python3

import matplotlib.pyplot as pyplot
import os
import pathlib
import subprocess
import sys

def get_files(path):
    files = []
    dirpath = pathlib.Path(path)
    if dirpath.is_dir() == False:
        if path.endswith('.ivf'):
            files.append(path)
        return files
    for entry in dirpath.iterdir():
        if entry.is_dir() == True:
            files += get_files(path + '/' + entry.name)
        elif entry.name.endswith('.ivf'):
            filename = path + '/' + entry.name
            files.append(filename)
    return files

def bench_run(args):
    fps = []
    load = []
    for i in range(3):
        p = subprocess.Popen(args, stderr=subprocess.PIPE)
        com = p.communicate()[1].decode()
        fps_idx_start = com.rfind('100.0%') + 10
        fps_idx_end = com[fps_idx_start:].find('/') + fps_idx_start
        fps.append(float(com[fps_idx_start:fps_idx_end]))
        load.append(int(com.splitlines()[-1][:-1]))
    fps = (fps[0] + fps[1] + fps[2]) / 3
    load = (load[0] + load[1] + load[2]) / 3
    return (fps, load)

def bench_configurations(filepath, master_threads, threads, limit, paths, master, vres):
    benches_ = []
    for i, p in enumerate(paths):
        b = []
        binpath = '/home/ubuntu/Projects/' + p + '/build/tools/dav1d'
        if master and i == 0:
            for num_threads in master_threads:
                confs = {
                    1:  (1, 1, 1),
                    3:  (1, 2, 1),
                    6:  (2, 2, 1),
                    10: (2, 4, 1),
                    15: (3, 4, 1),
                    20: (4, 4, 1),
                    30: (6, 4, 1),
                    40: (8, 4, 1),
                    54: (8, 6, 1),
                    72: (8, 8, 1),
                }
                ft = confs[num_threads][0]
                tt = confs[num_threads][1]
                args = ['/usr/bin/time', '-f', '%P', binpath,
                        '-i', filepath,
                        '--framethreads=' + str(ft),
                        '--tilethreads=' + str(tt),
                        '--limit=' + str(limit),
                        '--muxer=null']
                b.append(bench_run(args))
        else:
            for num_threads in threads:
                fd = 12 if vres < 1440 else 6
                args = ['/usr/bin/time', '-f', '%P', binpath,
                        '-i', filepath,
                        '--threads=' + str(num_threads),
                        '--framedelay=' + str(fd),
                        '--limit=' + str(int(limit)),
                        '--muxer=null']
                b.append(bench_run(args))
        benches_.append(b)
    benches = []
    for b in benches_:
        fps = []
        load = []
        for tup in b:
            fps.append(tup[0])
            load.append(tup[1])
        benches.append((load, fps))
    return benches

def bench_file(filepath, paths, master):
    filename = filepath[filepath.rfind('/')+1:]
    vres = filename[-8:-4]
    ldiv = 5
    limit = {
            '0480': int(50000 / ldiv),
            '0720': int(20000 / ldiv),
            '1080': int(10000 / ldiv),
            '1440': int( 5000 / ldiv),
            '2160': int( 2500 / ldiv),
            '4320': int( 1250 / ldiv)
    }[vres]
    print('%s: %d frames' % (filename, limit))
    master_threads = [1, 3, 6, 10, 15, 20, 30, 40, 54, 72]
    threads = [1, 2, 4, 8, 12, 16, 24, 32, 48, 64]
    benches = bench_configurations(filepath, master_threads, threads, limit, paths, master, int(vres))
    (fig, ax) = pyplot.subplots()
    colors = ['k', 'rg', 'rbg', 'rbcg', 'rmbcg', 'rmbcyg', 'krmbcyg']
    for i, b in enumerate(benches):
        ax.plot(b[0], b[1], colors[len(benches) - 1][i] + 'o-', label=paths[i][6:])
        if master and i == 0:
            for j, nt in enumerate(master_threads):
                ax.annotate(nt, (b[0][j], b[1][j]))
        else:
            for j, nt in enumerate(threads):
                ax.annotate(nt, (b[0][j], b[1][j]))
    pyplot.legend()
    pyplot.xlabel('System load')
    pyplot.ylabel('FPS')
    pyplot.grid(ls = '--', lw = .5)
    pyplot.suptitle(filename + ' (' + str(limit) + ' frames)')
    pyplot.savefig(filename + '.png', dpi = 600)

def main(ac, av):
    if ac < 3:
        return 1
    files = get_files(av[1])
    files.sort()
    paths = []
    for i in range(2, ac):
        paths.append(av[i])
    master = 0
    if paths[-1] == "master":
        paths = paths[:-1]
        master = 1
    if len(paths) > 7:
        print('error: can bench at most 7 versions')
        return 1
    for f in files:
        bench_file(f, paths, master)
    return 0

if __name__ == '__main__':
    sys.exit(main(len(sys.argv), sys.argv))
