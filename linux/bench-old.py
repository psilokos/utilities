#! /usr/bin/python3

import matplotlib.pyplot as pyplot
import os
import pathlib
import subprocess
import sys

def get_files(path):
    files = []
    dirpath = pathlib.Path(path)
    if dirpath.is_dir() == False:
        if path.endswith('.ivf'):
            files.append(path)
        return files
    for entry in dirpath.iterdir():
        if entry.is_dir() == True:
            files += get_files(path + '/' + entry.name)
        elif entry.name.endswith('.ivf'):
            filename = path + '/' + entry.name
            files.append(filename)
    return files

def bench_run(args):
    fps = []
    load = []
    for i in range(3):
        p = subprocess.Popen(args, stderr=subprocess.PIPE)
        com = p.communicate()[1].decode()
        fps_idx_start = com.rfind('100.0%') + 10
        fps_idx_end = com[fps_idx_start:].find('/') + fps_idx_start
        fps.append(float(com[fps_idx_start:fps_idx_end]))
        load.append(int(com.splitlines()[-1][:-1]))
    fps = (fps[0] + fps[1] + fps[2]) / 3
    load = (load[0] + load[1] + load[2]) / 3
    return (fps, load)

def bench_configurations(filepath, threads, limit, paths):
    benches_ = []
    for i, p in enumerate(paths):
        b = []
        binpath = '/home/ubuntu/Projects/' + p + '/build/tools/dav1d'
        for num_threads in threads:
            confs = {
                1:  (1, 1, 1),
                2:  (1, 2, 1),
                4:  (1, 2, 2),
                8:  (2, 3, 2),
                12: (3, 3, 3),
                16: (3, 4, 4),
                24: (4, 4, 8),
                32: (6, 4, 8),
                48: (6, 6, 12),
                64: (8, 6, 16),
            }
            ft = confs[num_threads][0]
            tt = confs[num_threads][1]
            pft = confs[num_threads][2]
            args = ['/usr/bin/time', '-f', '%P', binpath,
                    '-i', filepath,
                    '--framethreads=' + str(ft),
                    '--tilethreads=' + str(tt),
                    '--pfthreads=' + str(pft),
                    '--limit=' + str(limit),
                    '--muxer=null']
            b.append(bench_run(args))
        benches_.append(b)
    benches = []
    for b in benches_:
        fps = []
        load = []
        for tup in b:
            fps.append(tup[0])
            load.append(tup[1])
        benches.append((load, fps))
    return benches

def bench_file(filepath, paths):
    filename = filepath[filepath.rfind('/')+1:]
    vres = filename[-8:-4]
    ldiv = 5
    limit = {
            '0480': int(50000 / ldiv),
            '0720': int(20000 / ldiv),
            '1080': int(10000 / ldiv),
            '1440': int( 5000 / ldiv),
            '2160': int( 2500 / ldiv),
            '4320': int( 1250 / ldiv)
    }[vres]
    print('%s: %d frames' % (filename, limit))
    num_threads = [1, 2, 4, 8, 12, 16, 24, 32, 48, 64]
    benches = bench_configurations(filepath, num_threads, limit, paths)
    (fig, ax) = pyplot.subplots()
    colors = ['k', 'rg', 'rbg', 'rbcg', 'rmbcg', 'rmbcyg', 'krmbcyg']
    for i, b in enumerate(benches):
        ax.plot(b[0], b[1], colors[len(benches) - 1][i] + 'o-', label=paths[i][6:])
        for j, nt in enumerate(num_threads):
            if nt == 2:
                ax.annotate(3, (b[0][j], b[1][j]))
            else:
                ax.annotate(nt, (b[0][j], b[1][j]))
    pyplot.legend()
    pyplot.xlabel('System load')
    pyplot.ylabel('FPS')
    pyplot.grid(ls = '--', lw = .5)
    pyplot.suptitle(filename + ' (' + str(limit) + ' frames)')
    pyplot.savefig(filename + '.png', dpi = 600)

def main(ac, av):
    if ac < 3:
        return 1
    files = get_files(av[1])
    files.sort()
    paths = []
    for i in range(2, ac):
        paths.append(av[i])
    if len(paths) > 7:
        print('error: can bench at most 7 versions')
        return 1
    for f in files:
        bench_file(f, paths)
    return 0

if __name__ == '__main__':
    sys.exit(main(len(sys.argv), sys.argv))
