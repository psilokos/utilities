#! /usr/bin/python3

import os
import pathlib
import subprocess
import sys

def get_files(path):
    files = []
    dirpath = pathlib.Path(path)
    if dirpath.is_dir() == False:
        if path.endswith('.ivf'):
            files.append(path)
        return files
    for entry in dirpath.iterdir():
        if entry.is_dir() == True:
            files += get_files(path + '/' + entry.name)
        elif entry.name.endswith('.ivf'):
            filename = path + '/' + entry.name
            files.append(filename)
    return files

def bench_configuration(path, ft, tt, pfts, limit):
    print('    - ft=%d tt=%d' % (ft, tt))
    t0 = 0
    for pft in pfts:
        args = ['/usr/bin/time', '-f', '%e', './tools/dav1d',
                '-i', path,
                '--framethreads=' + str(ft),
                '--tilethreads=' + str(tt),
                '--pfthreads=' + str(pft),
                '--limit=' + str(limit),
                '--muxer=null', '-q']
        t = []
        for i in range(3):
            p = subprocess.Popen(args, stderr=subprocess.PIPE)
            t.append(float(p.communicate()[1]))
        t = (t[0] + t[1] + t[2]) / 3
        if pft == 1:
            t0 = t
            print('        pft=%-2d    %2.2f' % (pft, t))
        else:
            print('        pft=%-2d    %2.2f => %+2.2f%%' % (pft, t, 100 * t0 / t - 100))

def bench_file(path):
    filename = path[path.rfind('/')+1:]
    vres = filename[-8:-4]
    limit = {
            '0480': 50000,
            '0720': 20000,
            '1080': 10000,
            '1440': 5000,
            '2160': 2500,
            '4320': 1250
    }[vres]
    print('%s: %d frames' % (filename, limit))
    bench_configuration(path,  1, 8, [1, 2, 3, 4], limit)
    bench_configuration(path,  4, 8, [1, 2, 4, 6, 8], limit)
    bench_configuration(path,  8, 4, [1, 2, 4, 8, 12, 16], limit)
    bench_configuration(path, 12, 4, [1, 2, 4, 8, 12, 16, 24, 32], limit)
    print('-' * 80)

def main(ac, av):
    if ac == 1:
        return 1
    files = get_files(av[1])
    files.sort()
    for f in files:
        bench_file(f)
    return 0

if __name__ == '__main__':
    sys.exit(main(len(sys.argv), sys.argv))
