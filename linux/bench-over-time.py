#! /usr/bin/python3

import matplotlib.pyplot as pyplot
import os
import pathlib
import subprocess
import sys

import numpy as np
from scipy.interpolate import make_interp_spline, BSpline

def get_files(path):
    files = []
    dirpath = pathlib.Path(path)
    if dirpath.is_dir() == False:
        if path.endswith('.ivf') or path.endswith('.obu'):
            files.append(path)
        return files
    for entry in dirpath.iterdir():
        if entry.is_dir() == True:
            files += get_files(path + '/' + entry.name)
        elif entry.name.endswith('.ivf') or entry.name.endswith('.obu'):
            filename = path + '/' + entry.name
            files.append(filename)
    return files

def bench_run(args, limit):
    res = []
    p = subprocess.Popen(args, stderr=subprocess.PIPE)
    lines = p.communicate()[1].decode().splitlines()[1:]
    for l in lines:
        if l is lines[-1]:
            pos_start = l.find('==>>>') + 6
            pos_end = l.find('% of overhead')
            overhead = float(l[pos_start:pos_end])
        if not 'CPU load' in l and l is not lines[0]:
            continue
        n = int(l[8:l.find('/' + str(limit) + ' frames')])
        pos_start = l.find('-') + 2
        pos_end = l[pos_start:].find('/') + pos_start
        fps = float(l[pos_start:pos_end])
        if l is lines[0]:
            load = 0
        else:
            pos_start = l.find('CPU load') + 10
            load = int(l[pos_start:-1])
        res.append((n, int(round(fps)), load))
    return (res, overhead)

def bench(filepath, threads, limit, paths):
    benches = []
    for p in paths:
        args = ['/home/psilokos/Projects/' + p + '/build/tools/dav1d',
                '-i', filepath,
                '--threads=' + str(threads),
                '--limit=' + str(int(limit)),
                '--muxer=null']
        benches.append(bench_run(args, limit))
    return benches

def bench_file(filepath, paths):
    filename = filepath[filepath.rfind('/')+1:]
    vres = filename[-8:-4]
    ldiv = 1
    limit = {
            '0480': int(50000 / ldiv),
            '0540': int(40000 / ldiv),
            '0720': int(20000 / ldiv),
            '1080': int(10000 / ldiv),
            '1440': int( 5000 / ldiv),
            '2160': int( 2500 / ldiv),
            '4320': int( 1250 / ldiv)
    }[vres]
#   limit = 8929
    print('%s: %d frames' % (filename, limit))
    threads = 0
    benches = bench(filepath, threads, limit, paths)
    colors = ['k', 'rbg', 'rbg', 'rbcg', 'rmbcg', 'rmbcyg', 'krmbcyg']
    text_colors = [(1.0, 0.5, 0.2), (0.2, 0.5, 1.0), (0.25, 1.0, 0.25)]
# FPS graph
    (fig, ax) = pyplot.subplots()
    for i, res in enumerate(benches):
        b = res[0]
        overhead = res[1]
        n = np.array([tup[0] for tup in b])
        fps = [tup[1] for tup in b]
        load = [tup[2] for tup in b]
        new_n = np.linspace(n.min(), n.max(), 100)
        spl = make_interp_spline(n, fps, k=3)
        smooth_fps = spl(new_n)
        legend = paths[i][6:] + ' - overhead: ' + str(overhead) + '%'
        ax.plot(new_n, smooth_fps, colors[len(benches) - 1][i] + '-', label=legend)
        for j in range(int(len(b) / 10), len(b), int(len(b) / 10)):
            ax.annotate(str(load[j]) + '%', (n[j], fps[j]), color=text_colors[i], size=7)
    pyplot.legend()
    pyplot.xlabel('Frames decoded')
    pyplot.ylabel('Speed (fps)')
    pyplot.grid(ls = '--', lw = .5)
    pyplot.suptitle(filename + ' (' + str(limit) + ' frames) - FPS')
    pyplot.savefig(filename + '-fps.png', dpi = 600)
# SYSTEM LOAD graph
    (fig, ax) = pyplot.subplots()
    for i, res in enumerate(benches):
        b = res[0]
        overhead = res[1]
        n = np.array([tup[0] for tup in b])
        fps = [tup[1] for tup in b]
        load = [tup[2] for tup in b]
        new_n = np.linspace(n.min(), n.max(), 100)
        spl = make_interp_spline(n, load, k=3)
        smooth_load = spl(new_n)
        legend = paths[i][6:] + ' - overhead: ' + str(overhead) + '%'
        ax.plot(new_n, smooth_load, colors[len(benches) - 1][i] + '-', label=legend)
        for j in range(int(len(b) / 10), len(b), int(len(b) / 10)):
            ax.annotate(str(fps[j]) + 'fps', (n[j], load[j]), color=text_colors[i], size=5)
    pyplot.legend()
    pyplot.xlabel('Frames decoded')
    pyplot.ylabel('Load (%)')
    pyplot.grid(ls = '--', lw = .5)
    pyplot.suptitle(filename + ' (' + str(limit) + ' frames) - LOAD')
    pyplot.savefig(filename + '-load.png', dpi = 600)
# CSV file
    csv_file = open('bench.csv', 'w')
    print(filename + ', ' + str(limit), file=csv_file)
    for i, b in enumerate(benches):
        print(paths[i] + ', poc, fps, load', file=csv_file)
        for n, fps, load in b[0]:
            print(', ' + str(n) + ', ' + str(fps) + ', ' + str(load), file=csv_file)
    print('\nversion, overhead', file=csv_file)
    for i, b in enumerate(benches):
        print(paths[i] + ', ' + str(b[1]), file=csv_file)
    csv_file.close()

def main(ac, av):
    if ac < 3:
        return 1
    files = get_files(av[1])
    files.sort()
    paths = []
    for i in range(2, ac):
        paths.append(av[i])
    if len(paths) > 7:
        print('error: can bench at most 7 versions')
        return 1
    for f in files:
        bench_file(f, paths)
    return 0

if __name__ == '__main__':
    sys.exit(main(len(sys.argv), sys.argv))
