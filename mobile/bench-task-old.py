#! /usr/local/bin/python3

import matplotlib.pyplot as pyplot
import os
import pathlib
import subprocess
import sys
from ppadb.client import Client as AdbClient

def get_files(path):
    files = []
    dirpath = pathlib.Path(path)
    if dirpath.is_dir() == False:
        if path.endswith('.ivf') or path.endswith('.obu'):
            files.append(path[path.rfind('/')+1:])
        return files
    for entry in dirpath.iterdir():
        if entry.is_dir() == True:
            files += get_files(path + '/' + entry.name)
        elif entry.name.endswith('.ivf') or entry.name.endswith('.obu'):
            files.append(entry.name)
    return files

def bench_run(cmd, dev):
    fps = []
    load = []
    num_runs = 5
    for i in range(num_runs):
        output = dev.shell(cmd)
        lines = output.splitlines()[-3:-1]
        fps_idx_start = output.rfind('100.0%') + 10
        fps_idx_end = output[fps_idx_start:].find('/') + fps_idx_start
        fps.append(float(output[fps_idx_start:fps_idx_end]))
        load.append(100 * float(lines[1][5:]) / float(lines[0][5:]))
    return (sum(fps) / num_runs, sum(load) / num_runs)

def bench_configurations(filename, master_threads, threads, limit, paths, master, dev):
    filepath = '/sdcard/Movies/AV1/' + filename
    benches_ = []
    for i, p in enumerate(paths):
        dav1d_path = '/data/local/tmp/' + p
        binpath = dav1d_path + '/bin/dav1d'
        ld_libpath = 'LD_LIBRARY_PATH="' + dav1d_path + '/lib"'
        b = []
        if master and i == 0:
            for conf in master_threads:
                cmd = ld_libpath + ' time ' + binpath + ' -i ' + filepath
                confs = {
                    '1f1t1p': (1, 1, 1),
                    '2f2t1p': (2, 2, 1),
                    '2f4t1p': (2, 4, 1),
                    '4f4t2p': (4, 4, 2),
                    '8f4t2p': (8, 4, 2),
                }
                ft  = confs[conf][0]
                tt  = confs[conf][1]
                pft = confs[conf][2]
                cmd += ' --framethreads=' + str(ft)
                cmd += ' --tilethreads=' + str(tt)
                cmd += ' --pfthreads=' + str(pft)
                cmd += ' --limit=' + str(int(limit))
                cmd += ' --muxer=null'
                b.append(bench_run(cmd, dev))
        else:
            for nt in threads:
                cmd = ld_libpath + ' time ' + binpath + ' -i ' + filepath
                cmd += ' --threads=' + str(nt)
                cmd += ' --limit=' + str(int(limit))
                cmd += ' --muxer=null'
                b.append(bench_run(cmd, dev))
        benches_.append(b)
    benches = []
    for b in benches_:
        fps = []
        load = []
        for tup in b:
            fps.append(tup[0])
            load.append(tup[1])
        benches.append((load, fps))
    return benches

def bench_file(filename, paths, master, dev):
    vres = filename[-8:-4]
    ldiv = 10
    limit = {
            '0480': int(50000 / ldiv),
            '0540': int(40000 / ldiv),
            '0720': int(20000 / ldiv),
            '1080': int(10000 / ldiv),
            '1440': int( 5000 / ldiv),
            '2160': int( 2500 / ldiv),
            '4320': int( 1250 / ldiv)
    }[vres]
    print('%s: %d frames' % (filename, limit))
    master_threads = ['1f1t1p', '2f2t1p', '2f4t1p', '4f4t2p', '8f4t2p']
    threads = [1, 2, 3, 4, 6, 8]
    benches = bench_configurations(filename, master_threads, threads, limit, paths, master, dev)
    (fig, ax) = pyplot.subplots()
    colors = ['k', 'rg', 'rbg', 'rbcg', 'rmbcg', 'rmbcyg', 'krmbcyg']
    for i, b in enumerate(benches):
        ax.plot(b[0], b[1], colors[len(benches) - 1][i] + 'o-', label=paths[i][6:])
        if master and i == 0:
            for j, conf in enumerate(master_threads):
                ax.annotate(conf, (b[0][j], b[1][j]))
        else:
            for j, nt in enumerate(threads):
                ax.annotate(nt, (b[0][j], b[1][j]))
    pyplot.legend()
    pyplot.xlabel('System load')
    pyplot.ylabel('FPS')
    pyplot.grid(ls = '--', lw = .5)
    pyplot.suptitle(filename + ' (' + str(limit) + ' frames)')
    pyplot.savefig(filename + '.png', dpi = 600)

def main(ac, av):
    if ac < 3:
        return 1
    files = get_files(av[1])
    files.sort()
    paths = []
    for i in range(2, ac):
        paths.append(av[i])
    master = 0
    if paths[-1] == "master":
        paths = paths[:-1]
        master = 1
    if len(paths) > 7:
        print('error: can bench at most 7 versions')
        return 1
    dev = AdbClient(host="127.0.0.1", port=5037).devices()[0]
    for f in files:
        bench_file(f, paths, master, dev)
    return 0

if __name__ == '__main__':
    sys.exit(main(len(sys.argv), sys.argv))
