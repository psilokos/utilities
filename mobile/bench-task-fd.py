#! /opt/local/bin/python

import matplotlib.pyplot as pyplot
import os
import pathlib
import subprocess
import sys
import time
from ppadb.client import Client as AdbClient

def bench_run(cmd, dev):
    fps = []
    load = []
    num_runs = 5
    for i in range(num_runs):
        output = dev.shell(cmd)
        lines = output.splitlines()[-3:-1]
        fps_idx_start = output.rfind('100.0%') + 10
        fps_idx_end = output[fps_idx_start:].find('/') + fps_idx_start
        fps.append(float(output[fps_idx_start:fps_idx_end]))
        load.append(100 * float(lines[1][5:]) / float(lines[0][5:]))
    return (sum(fps) / num_runs, sum(load) / num_runs)

def bench_configurations(filepath, threads, limit, framedelays, path, master, dev):
    benches_ = []
    for i, fd in enumerate(framedelays):
        dav1d_path = '/data/local/tmp/' + path
        binpath = dav1d_path + '/bin/dav1d'
        ld_libpath = 'LD_LIBRARY_PATH="' + dav1d_path + '/lib"'
        b = []
        for num_threads in threads:
            cmd = ld_libpath + ' time ' + binpath + ' -i ' + filepath
            cmd += ' --threads=' + str(num_threads)
            cmd += ' --limit=' + str(int(limit))
            if fd == -1:
                cmd += ' --framedelay=' + str(num_threads)
            else:
                cmd += ' --framedelay=' + str(fd)
            cmd += ' --muxer=null'
            b.append(bench_run(cmd, dev))
        benches_.append(b)
    benches = []
    for b in benches_:
        fps = []
        load = []
        for tup in b:
            fps.append(tup[0])
            load.append(tup[1])
        benches.append((load, fps))
    return benches

def bench_file(filepath, path, master, dev):
    filename = filepath[filepath.rfind('/')+1:]
    vres = filename[-8:-4]
    ldiv = 10
    limit = {
            '0480': int(50000 / ldiv),
            '0720': int(20000 / ldiv),
            '1080': int(10000 / ldiv),
            '1440': int( 5000 / ldiv),
            '2160': int( 2500 / ldiv),
            '4320': int( 1250 / ldiv)
    }[vres]
    print('%s: %d frames' % (filename, limit))
    num_threads = [4, 8, 16, 32]
    framedelays = [-1, 2, 4, 6, 8]
    benches = bench_configurations(filepath, num_threads, limit, framedelays, path, master, dev)
    (fig, ax) = pyplot.subplots()
    colors = ['k', 'rg', 'rbg', 'rbcg', 'mbcgr', 'rmbcyg', 'krmbcyg']
    for i, b in enumerate(benches):
        ax.plot(b[0], b[1], colors[len(benches) - 1][i] + 'o-', label=framedelays[i])
        for j, nt in enumerate(num_threads):
            if master and i == 0 and nt == 2:
                ax.annotate(3, (b[0][j], b[1][j]))
            else:
                ax.annotate(nt, (b[0][j], b[1][j]))
    pyplot.legend()
    pyplot.xlabel('System load')
    pyplot.ylabel('FPS')
    pyplot.grid(ls = '--', lw = .5)
    pyplot.suptitle(filename + ' (' + str(limit) + ' frames)')
    pyplot.savefig(filename + '.png', dpi = 600)

def main(ac, av):
    if ac < 3:
        return 1
    paths = []
    for i in range(2, ac):
        paths.append(av[i])
    master = 0
    if paths[-1] == "master":
        paths = paths[:-1]
        master = 1
    if len(paths) > 7:
        print('error: can bench at most 7 versions')
        return 1
    dev = AdbClient(host="127.0.0.1", port=5037).devices()[0]
    for p in paths:
        bench_file(av[1], p, master, dev)
    return 0

if __name__ == '__main__':
    sys.exit(main(len(sys.argv), sys.argv))
