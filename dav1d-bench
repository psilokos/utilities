#! /bin/bash

function usage
{
    echo 'usage: dav1d-bench [-f] [-r X] [-p] [-s X] [-l X] [-t] [-w X] <-g revision [-m mask] | -m mask_ref -m mask_cmp> <file|directory>...'
    echo '  -f          fast option - skip tests before benchmarking'
    echo '  -r X        repeat benchmark X times'
    echo '  -d          compute the standard deviation (only with -r)'
    echo '  -p          run perf record'
    echo '  -s X        skip X frames'
    echo '  -l X        decode X frames only'
    echo '  -t          enable multithreading'
    echo '  -w X        X warm up runs before bench'
    echo '  -z X        sleep for X seconds between each run'
    echo '  -g gitrev   bench gitrev against HEAD'
    echo '  -m mask     cpumask to use, one of sse2, ssse3, sse41, avx2, avx512icl (defaults to 0, no optimized assembly)'
    exit $1
}

function fail
{
    if [ $onbranch -eq 0 ]; then
        git checkout -q $branch
    else
        git checkout -q $head
    fi
    echo $1 1>&2
    exit 1
}

function prep_rev
{
    git checkout -q $1
    echo -n building $1...
    ninja -j 8 &>/dev/null
    if [ $? -ne 0 ]; then
        echo ' KO'; fail "build failed for $1"
    fi
    echo ' OK'
    if [ $skip_tests -eq 0 ]; then
        echo -n testing $1...
        ninja test &>/dev/null
        if [ $? -ne 0 ]; then
            echo ' KO'; fail "tests failed for $1"
        fi
        echo ' OK'
    fi
}

function compute_stddev
{
    t_stddev=0
    for ((i=0;i<${#t[@]};i++)); do
        t_stddev=`echo "$t_stddev + (${t[$i]}-$t_mean)^2" | bc -l`
    done
    t_stddev=`echo "sqrt($t_stddev/${#t[@]})" | bc -l`
    stddevs+=($t_stddev)
}

function bench_file
{
    if [ $perf -eq 0 ]; then
        echo; echo "running $repeat time(s) (with $warm_up_runs warm up runs / sleeping for $sleep_time seconds between each run):"
        echo "./tools/dav1d --cpumask=$cpumask --framethreads=$framethreads --tilethreads=$tilethreads -i $1 --muxer null -s $skip -l $limit"
        for ((i=0;i<$warm_up_runs;i++)); do
            ./tools/dav1d --cpumask=$cpumask --framethreads=$framethreads --tilethreads=$tilethreads -i $1 --muxer null -s $skip -l $limit -q 2>/dev/null 2>&1
        done
        t=()
        sum=0
        for ((i=0;i<$repeat;i++)); do
            if [ $sleep_time -ne 0 ]; then
                sleep $sleep_time
            fi
            curtime=`$timebin -f "%e" ./tools/dav1d --cpumask=$cpumask --framethreads=$framethreads --tilethreads=$tilethreads -i $1 --muxer null -s $skip -l $limit -q 2>/dev/null 2>&1`
            t+=($curtime)
            sum=`echo $sum + $curtime | bc -l`
        done
        t_mean=`echo $sum / $repeat | bc -l`
        if [ $stddev -eq 1 ]; then
            compute_stddev
            echo "=> $t_mean seconds (stdev=$t_stddev)"
        else
            echo "=> $t_mean seconds"
        fi
        timings+=($t_mean)
    else
        echo "running: perf stat -r $repeat ./tools/dav1d --cpumask=$cpumask --framethreads=1 --tilethreads=1 -i $1 --muxer null -s $skip -l $limit"
        perf stat -r $repeat -d ./tools/dav1d --cpumask=$cpumask --framethreads=1 --tilethreads=1 -i $1 --muxer null -s $skip -l $limit 2>&1
    fi
}

function bench_rev
{
    prep_rev $1
    benched_thingies+=($1)
    for f in ${files[@]}; do
        bench_file $f
    done
    echo; echo --------------------------------------------------------------------------------; echo
}

function bench_mask
{
    echo; echo benching CPU capability: $1
    benched_thingies+=($1)
    cpumask=$1
    for f in ${files[@]}; do
        bench_file $f
    done
    echo; echo --------------------------------------------------------------------------------; echo
}

function end
{
    if [ $onbranch -eq 0 ]; then
        git checkout -q $branch
    fi
    exit 0
}

skip_tests=0
repeat=1
stddev=0
perf=0
skip=0
limit=0
framethreads=1
tilethreads=1
warm_up_runs=0
sleep_time=0
gitrev=HEAD
cpumask_ref=-1
cpumask_cmp=-1
while getopts "hfr:dps:l:tw:z:g:m:" opt; do
    case "${opt}" in
        h)
            usage 0
            ;;
        f)
            skip_tests=1
            ;;
        r)
            repeat=$OPTARG
            if [ $repeat -lt 1 ]; then
                echo "-r cannot be less than 1" 1>&2
                exit 1
            fi
            ;;
        d)  stddev=1
            ;;
        p)
            perf=1
            ;;
        s)
            skip=$OPTARG
            ;;
        l)
            limit=$OPTARG
            ;;
        t)
            framethreads=8
            tilethreads=4
            ;;
        w)  if [ $sleep_time -ne 0 ]; then
                echo "running warm up runs and sleeping is useless" 1>&2
                exit 1
            fi
            warm_up_runs=$OPTARG
            ;;
        z)  if [ $warm_up_runs -ne 0 ]; then
                echo "running warm up runs and sleeping is useless" 1>&2
                exit 1
            fi
            sleep_time=$OPTARG
            ;;
        g)  gitrev=$OPTARG
            ;;
        m)
            case "$OPTARG" in
                "sse2"|"ssse3"|"sse41"|"avx2"|"avx512icl")
                    if [ "$gitrev" != "HEAD" ] || [ "$cpumask_ref" == "-1" ]; then
                        if [ "$cpumask_ref" != "-1" ]; then
                            echo "sorry bud, cannot bench two revisions and CPU masks against each other" 1>&2
                            exit 1
                        fi
                        cpumask_ref=$OPTARG
                    else
                        if [ "$cpumask_cmp" != "-1" ]; then
                            echo "cannot 3 CPU masks against each other" 1>&2
                            exit 1
                        fi
                        cpumask_cmp=$OPTARG
                    fi
                    ;;
                *)
                    usage 1
                    ;;
            esac
            ;;
        *)
            usage 1
            ;;
    esac
done
if [ $repeat -eq 1 ]; then
    stddev=0
fi
shift "$((OPTIND-1))"
if test $# -lt 1; then
    usage 1
fi

case "$(uname -s)" in
    Linux*)  timebin='/usr/bin/time';;
    Darwin*) timebin='gtime';;
    *)       fail "unknow system, exiting..."
esac

if test -n "`git status --porcelain`"; then
    echo "your git is not clean, please commit stash or abort your changes to run this script" 1>&2
    exit 1
fi

test "`git status -b | awk 'NR==1{print $1;}'`" != "HEAD"
onbranch=$?
if [ $onbranch -eq 0 ]; then
    branch=`git status -b | awk 'NR==1{print $3;}'`
fi

for i in $@; do
    if test -d $i; then
        for f in $i/*.ivf; do
            files+=($f)
        done
    else
        files+=($i)
    fi
done

head=`git rev-parse HEAD`
if [ "$cpumask_cmp" == "-1" ]; then
    target=`git rev-parse $gitrev`
    if [ "$cpumask_ref" == "-1" ]; then
        cpumask=0
    else
        cpumask=$cpumask_ref
    fi
    bench_rev $target
    if [ "$target" == "$head" ]; then
        end
    fi
    bench_rev $head
    if [ $perf -eq 1 ]; then
        end
    fi
else
    prep_rev $head
    bench_mask $cpumask_ref
    bench_mask $cpumask_cmp
fi

echo relative decode performance report:
if [ $stddev -eq 1 ]; then echo; fi
cnt_per_file=${#benched_thingies[@]}
filecnt=${#files[@]}
for ((i=0;i<$filecnt;i++)); do
    echo; echo "- ${files[$i]}:"
    prev_t=${timings[0]}
    for ((j=1;j<$cnt_per_file;j++)); do
        idx=$((j*filecnt))
        t=${timings[$idx]}
        diff=`echo $prev_t \* 100 / $t - 100 | bc -l`
        prev_t=$t
        echo -e "\t${benched_thingies[$j-1]}..${benched_thingies[$((j))]} = $diff%"
        if [ $stddev -eq 1 ]; then
            idx0=$(((j-1)*filecnt))
            echo "    stddev_${benched_thingies[$j-1]}=${stddevs[$idx0]}"
            echo "    stddev_${benched_thingies[$j]}=${stddevs[$idx]}"; echo
        fi
    done
    timings=(${timings[@]:1})
    if [ $stddev -eq 1 ]; then
        stddevs=(${stddevs[@]:1})
    fi
done

end
