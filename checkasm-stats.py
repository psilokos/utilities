#! /usr/bin/python3

import collections
import statistics
import subprocess
import sys

def bench(args, benches):
    ps = subprocess.run(args, stdout=subprocess.PIPE, stderr=subprocess.DEVNULL)
    for line in ps.stdout.splitlines():
        line = line.decode()
        bench_line = False
        for i, s in enumerate(suffixes):
            bench_line = s in line
            if bench_line:
                break
        if not bench_line:
            continue
        value = float(line[line.find(':') + 2:])
        ext = s[1:-1]
        if ext not in benches:
            benches[ext] = [value]
        else:
            benches[ext].append(value)

def analyze(benches):
    speed_ups0 = {}
    speed_diffs0 = {}
    benches_list = [l for l in benches.items()]
    for i in range(len(benches) - 1):
        key = benches_list[i][0] + '..' + benches_list[i+1][0]
        speed_ups0[key] = [benches_list[i][1][j] / benches_list[i+1][1][j] for j in range(len(benches_list[0][1]))]
        speed_diffs0[key] = [benches_list[i+1][1][j] / benches_list[i][1][j] for j in range(len(benches_list[0][1]))]
    speed_ups = {}
    speed_diffs = {}
    for key, vals in speed_ups0.items():
        speed_ups[key] = (statistics.mean(vals), statistics.stdev(vals))
    for key, vals in speed_diffs0.items():
        speed_diffs[key] = (statistics.mean(vals), statistics.stdev(vals))
    return speed_ups, speed_diffs

def measure(runs, benches, args):
    for i in range(runs):
        bench(args, benches)
    return analyze(benches)

suffixes = ['_c:', '_ssse3:', '_avx2:', '_avx512icl:']
args = sys.argv[1:]

benches = collections.OrderedDict()
threshold = 0.005
num_benches = 0
runs = 3
rerun = True
while rerun:
    speed_ups, speed_diffs = measure(runs, benches, args)
    rerun = False
    for key, (_, stdev) in speed_diffs.items():
        if stdev > threshold:
            benches.clear()
            rerun = True
            print(f'{key} stdev {stdev} > {threshold}')
            print(f'rerunning {runs} times')
            break

print('--------------------- FINAL ---------------------')
print('Speed ups:')
for key, (mean, stdev) in speed_ups.items():
    mean = round(mean, 3)
    stdev = round(stdev, 4)
    print(f'{key}: {mean}x (o={stdev})')
print('Speed diffs:')
for key, (mean, stdev) in speed_diffs.items():
    mean = round(mean * 100, 2)
    stdev = round(stdev * 100, 2)
    print(f'{key}: {mean}% (o={stdev})')
