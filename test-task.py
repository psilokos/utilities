#! /usr/bin/python3

import os
import pathlib
import subprocess
import sys

def get_files(path):
    files = []
    dirpath = pathlib.Path(path)
    if dirpath.is_dir() == False:
        if path.endswith('.ivf'):
            files.append(path)
        return files
    for entry in dirpath.iterdir():
        if entry.is_dir() == True:
            files += get_files(path + '/' + entry.name)
        elif entry.name.endswith('.ivf'):
            filename = path + '/' + entry.name
            files.append(filename)
    return files

def test_configuration(path, filename, num_threads, limit):
    if limit != 0:
        print('%d FRAMES' % (limit))
    else:
        print('ALL FRAMES')
    args = ['../../dav1d-clean/build/tools/dav1d',
            '-i', path,
            '--framethreads=8',
            '--tilethreads=' + str(num_threads),
            '--limit=' + str(limit),
            '-o', filename + '.md5']
    p = subprocess.Popen(args)
    p.wait()
    p = subprocess.Popen(['cat', filename + '.md5'], stdout=subprocess.PIPE)
    md5 = str(p.communicate()[0])[2:-3]
    args = ['./tools/dav1d',
            '-i', path,
            '--threads=' + str(num_threads),
            '--limit=' + str(limit),
            '--verify=' + md5]
    p = subprocess.Popen(args)
    p.wait()
    if p.returncode != 0:
        print('sorry, but you failed (%d)\n' % (p.returncode))

def test_file(path):
    filename = path[path.rfind('/')+1:]
    vres = filename[-8:-4]
    ldiv = 1
    limit = {
            '0480': 50000 / ldiv,
            '0720': 20000 / ldiv,
            '1080': 10000 / ldiv,
            '1440': 5000 / ldiv,
            '2160': 2500 / ldiv,
            '4320': 1250 / ldiv
    }[vres]
    print('%s:' % (filename))
    test_configuration(path, filename, 12, int(limit))
    test_configuration(path, filename, 12, 0)
    print('')

def main(ac, av):
    if ac == 1:
        return 1
    files = get_files(av[1])
    files.sort()
    for f in files:
        test_file(f)
    return 0

if __name__ == '__main__':
    sys.exit(main(len(sys.argv), sys.argv))
