#! /usr/bin/sh

set -e
sudo pip3 install matplotlib
rm -rf dav1d
git clone git@github-twoorioles:twoorioles/dav1d.git dav1d-base
cd dav1d-base
git checkout $1
git clone https://code.videolan.org/videolan/dav1d-test-data.git tests/dav1d-test-data
git worktree add ../dav1d-master gitlab-master
if [[ $# -eq 2 ]]; then
    git worktree add ../dav1d-tmp $2
elif [[ $# -eq 3 ]]; then
    git worktree add ../$3 $2
fi
cd ../dav1d-master
meson build
ninja -j80 -C build
if [[ $# -eq 2 ]]; then
    cd ~/Projects/dav1d-tmp
    meson build
    ninja -j80 -C build
elif [[ $# -eq 3 ]]; then
    cd ../$3
    meson build
    ninja -j80 -C build
fi
cd ../dav1d-base
meson build
ninja -j80 -C build
