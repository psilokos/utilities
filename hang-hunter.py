#! /usr/bin/python3

import os
import pathlib
import subprocess
import sys

def get_files(path):
    files = []
    dirpath = pathlib.Path(path)
    if dirpath.is_dir() == False:
        if path.endswith('.ivf') or path.endswith('.obu'):
            files.append(path)
        return files
    for entry in dirpath.iterdir():
        if entry.is_dir() == True:
            files += get_files(path + '/' + entry.name)
        elif entry.name.endswith('.ivf') or entry.name.endswith('.obu'):
            filename = path + '/' + entry.name
            files.append(filename)
    return files

def test_configuration(path, threads, framedelay):
    args = ['./tools/dav1d',
            '-i', path,
            '--threads=' + str(threads),
            '--framedelay=' + str(framedelay),
            '--muxer=null']
    p = subprocess.Popen(args, stdout = subprocess.PIPE)
    for line in p.stdout:
        print(line)

def main(ac, av):
    if ac == 1:
        return 1
    files = get_files(av[1])
    files.sort()
    while True:
        for threads in range(1, 64+1):
            for framedelay in range(1, min(threads+1, 64+1)):
                print('Decoding all files with --threads=%d --framedelay=%d:' % (threads, framedelay))
                for path in files:
                    print('%s:' % (path[path.rfind('/')+1:]))
                    test_configuration(path, threads, framedelay)
                    print('')
                print('*' * 80)
    return 0

if __name__ == '__main__':
    sys.exit(main(len(sys.argv), sys.argv))
